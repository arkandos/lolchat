define(["jquery", "knockout"], function($, ko) {

	var RIOT_API_KEY = "fd4aac5a-1c70-4040-8297-9db884cf78d2";

	function Summoner(id, region) {
		this.id = id;
		this.region = region;

		this.name = ko.observable("");
		this.level = ko.observable(0);
		this.profileIcon = ko.observable(0);
		this.normalWins = ko.observable(0); // can't figure this one out using the api, but we get it sometimes from the chat

		this.leagueName = ko.observable("");
		this.tier = ko.observable("UNRANKED");
		this.division = ko.observable("??");
		this.leaguePoints = ko.observable(0);
		this.rankedWins = ko.observable(0);
		this.leagueEntries = ko.observableArray([]);
	};

	Summoner.searchByName = function(summonerName, region, callback) {

		var internalCallback = function(response) {
			for(var normalizedName in response) if(response.hasOwnProperty(normalizedName)) {
				var summonerData = response[normalizedName];
				if(summonerData["name"] == summonerName) {
					var summoner = new Summoner(summonerData["id"], region);

					summoner.name(summonerData["name"]);
					summoner.level(summonerData["summonerLevel"]);
					summoner.profileIcon(summonerData["profileIconId"]);

					callback(summoner);

					break;
				}
			}
		};

		$.ajax({
			url: region.api + "/v1.4/summoner/by-name/" + summonerName + "?api_key=" + RIOT_API_KEY,
			dataType: "json",
			success : internalCallback,
		});
	};

	Summoner.prototype.updateSummonerData = function(callback) {

		var internalCallback = function(response) {
			this.name(response[this.id]["name"]);
			this.level(response[this.id]["summonerLevel"]);
			this.profileIcon(response[this.id]["profileIconId"]);
			if(callback) callback(this);
		};

		$.ajax({
			url: this.region.api + "/v1.4/summoner/" + this.id + "?api_key=" + RIOT_API_KEY,
			dataType: "json",
			success: internalCallback.bind(this),
		});
	};

	Summoner.prototype.updateLeagueData = function(callback) {

		var internalCallback = function(response) {
			for(var i = 0; i < response[this.id].length; ++i) {
				var leagueEntry = response[this.id][i];
				if(leagueEntry.queue == "RANKED_SOLO_5x5") {

					var leagueEntryEntries = [];

					for(var j = 0; j < leagueEntry.entries.length; ++j) {
						var entryJson = leagueEntry.entries[j], entrySummoner = null;

						if(entryJson.playerOrTeamId == this.id) {
							entrySummoner = this;
						} else {
							entrySummoner = new Summoner(entryJson.playerOrTeamId, this.region);
						}

						entrySummoner.leagueName(leagueEntry.name);
						entrySummoner.leaguePoints(entryJson.leaguePoints);
						entrySummoner.division(entryJson.division);
						entrySummoner.tier(leagueEntry.tier);
						entrySummoner.name(entryJson.playerOrTeamName);
						entrySummoner.rankedWins(entryJson.wins);

						leagueEntryEntries.push(entrySummoner);
					}

					leagueEntryEntries.sort(function(a, b) {
						if(a.division() == b.division()) return a.leaguePoints() < b.leaguePoints();
						else return a.division() < b.division();
					});

					this.leagueEntries(leagueEntryEntries);

					break;
				}
			}
			if(callback) callback(this);
		};

		$.ajax({
			url: this.region.api + "/v2.5/league/by-summoner/" + this.id + "?api_key=" + RIOT_API_KEY,
			dataType: "json",
			success: internalCallback.bind(this)
		});
	};

	Summoner.prototype.updateMasteries = function(callback) {
		if(callback) callback(this);
	};

	Summoner.prototype.updateRunes = function(callback) {
		if(callback) callback(this);
	};

	Summoner.prototype.updateAll = function(callback) {
		var updateCounter = 0;
		var updateFns = [ this.updateSummonerData, this.updateLeagueData, this.updateMasteries, this.updateRunes ];

		var internalCallback = function() {
			updateCounter ++;
			if(callback && updateCounter >= updateFns.length) {
				callback();
			}
		};

		for(var i =0; i < updateFns.length; ++i) {
			var fn = updateFns[i];
			fn.call(this, internalCallback);
		}
	};

	return Summoner;
});
