define(["jquery", "knockout", "./router", "bootstrap"], function($, ko, router) {

	ko.components.register("nav-bar", { require: "components/nav-bar/nav-bar" });
	ko.components.register("alert", { require: "components/alert/alert" });
	ko.components.register("home-page", { require: "components/home-page/home-page" });
	ko.components.register("chat-page", { require: "components/chat-page/chat-page" });
	ko.components.register("profile-page", { require: "components/profile-page/profile-page" });
	ko.components.register("about-page", { template: { require: "text!components/about-page/about-page.html" } });

	ko.extenders.localStorage = function(target, localStorageKey) {

		if(!localStorageKey) return target;

		var initialValue = null;
		if(localStorage.hasOwnProperty(localStorageKey)) {
			try {
				initialValue = JSON.parse(localStorage.getItem(localStorageKey));
			} catch(e) {}
		}

		if(initialValue !== null) {
			target(initialValue);
		}

		target.subscribe(function(newValue) {
			localStorage.setItem(localStorageKey, ko.toJSON(newValue));
		});

		return target;
	};

	ko.bindingHandlers.collapsed = {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
			var value = valueAccessor(),
				valueUnwrapped = ko.unwrap(value);

			//$(element).toggleClass("in", !valueUnwrapped);

			if(ko.isObservable(value)) {
				ko.utils.registerEventHandler(element, "shown.bs.collapse", function(event) {
					value(false);
				});
				ko.utils.registerEventHandler(element, "hidden.bs.collapse", function(event) {
					value(true);
				});
			}
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
			var collapsed = ko.unwrap(valueAccessor());
			$(element).collapse({ toggle: collapsed });
		},
	};

	function AppModel() {
		this.route = router.currentRoute;
		this.activeConnections = ko.observableArray([]);
	};

	window.appModel = new AppModel();

	ko.applyBindings(window.appModel);

});

