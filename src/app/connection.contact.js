define(["jquery", "knockout", "strophe", "app/summoner"], function($, ko, Strophe, Summoner) {
	// CLASS Connection.Contact

	var SHOW_MESSAGES = { chat: "Online", dnd: "Busy", away: "Away" };
	var GAME_STATUS_MESSAGES = { outOfGame: "Online", inGame: "In Game", championSelect: "In Champion Select" };

	function Contact(region, jid, options) {
		var defaults = { name: "", show: "offline", group: "**Default", subscription: "both" };

		//this.connection = connection;

		this.jid = jid;
		this.region = region;

		this.summoner = new Summoner(Contact.summonerIdForJid(jid), region);
		this.summoner.name(options.name || defaults.name);

		this.status = ko.pureComputed({
			read: this.createStatusMessage,
			write: this.parseStatusMessage,
			owner: this,
		});

		this.show = ko.observable(options.show || defaults.show);
		this.available = ko.pureComputed(function() {
			return (this.show() == "chat");
		}, this);

		this.statusMessage = ko.observable("");
		this.statusInformation = ko.pureComputed(function() {
			if(this.statusMessage().length > 0) {
				return this.statusMessage();
			} else if(this.gameStatus() != "outOfGame") {
				return GAME_STATUS_MESSAGES[ this.gameStatus() ] || this.gameStatus();
			} else {
				return SHOW_MESSAGES[ this.show() ] || this.show();
			}
		}, this);

		this.presence = ko.pureComputed({
			read: this.createPresence,
			write: this.parsePresence,
			owner: this,
		});

		this.gameStatus = ko.observable("outOfGame"); //TODO figure out states
		this.queueType = ko.observable("");

		this.group = ko.observable(options.group || defaults.group);
		this.groupObject = ko.pureComputed(function() {
			return ko.utils.arrayFirst(ko.unwrap(connection._allGroups), function(group) {
				return group.name() == this.group();
			}.bind(this));
		}, this);

		this.subscription = ko.observable(options.subscription || defaults.subscription);
	};

	//Contact.RIOT_API_KEY = "fd4aac5a-1c70-4040-8297-9db884cf78d2";

	Contact.summonerIdForJid = function(jid) {
		return parseInt( /^sum([0-9]+)@pvp\.net(\/.+)?$/.exec(jid)[1] );
	};
	Contact.bySummonerId = function(id) {
		return function(contact) {
			return contact.summoner.id == id;
		};
	};
	Contact.byJid = function(jid) {
		return function(contact) {
			return (Strophe.getBareJidFromJid(contact.jid) == Strophe.getBareJidFromJid(jid));
		};
	}

	Contact.prototype.updateSummoner = function(callback) {
		this.summoner.updateSummonerData(callback);
	};

	Contact.prototype.createStatusMessage = function() {
		var statusXml = new Strophe.Builder("body");

		statusXml
			.c("profileIcon").t(this.summoner.profileIcon()).up()
			.c("level").t(this.summoner.level()).up();
			//.c("wins").t(this.summoner.normalWins()).up()

		if(this.summoner.tier() != "UNRANKED") {
			statusXml
				.c("rankedRating").t(this.summoner.leaguePoints()).up()
				.c("rankedLeagueTier").t(this.summoner.tier()).up()
				.c("rankedLeagueName").t(this.summoner.leagueName()).up()
				.c("rankedLeagueDivision").t(this.summoner.division()).up()
				.c("rankedWins").t(this.summoner.rankedWins()).up()

				.c("rankedLeagueQueue").t("RANKED_SOLO_5x5").up();
		}

		statusXml
			.c("statusMsg").t(this.statusMessage()).up()
			.c("gameStatus").t(this.gameStatus()).up()
			.c("queueType").t(this.queueType()).up();

		return statusXml.toString();
	};

	Contact.prototype.parseStatusMessage = function(status) {
		var statusXml = $.parseXML(status),
			$statusXml = $(statusXml);

		this.summoner.profileIcon( $statusXml.find("profileIcon").text() );
		this.summoner.level( $statusXml.find("level").text() );
		this.summoner.normalWins( $statusXml.find("wins").text() );
		this.summoner.leaguePoints( $statusXml.find("rankedRating").text() );
		this.summoner.tier( $statusXml.find("rankedLeagueTier").text() );
		this.summoner.leagueName( $statusXml.find("rankedLeagueName").text() );
		this.summoner.division( $statusXml.find("rankedLeagueDivision").text() );
		this.summoner.rankedWins( $statusXml.find("rankedWins").text() );

		this.statusMessage( $statusXml.find("statusMsg").text() );
		this.gameStatus( $statusXml.find("gameStatus").text() );
		this.queueType( $statusXml.find("queueType").text() );

	};

	Contact.prototype.createPresence = function() {
		var status = $pres();
		status
			.c("show").t(this.show()).up()
			.c("status").t(this.status()).up()
			.c("priority").t("0").up(); // is this necessary?

		return status;
	};

	Contact.prototype.parsePresence = function(presence) {
		if($(presence).attr("type") == "unavailable") {
			this.show("offline");
		} else {
			this.show($(presence).find("show").text());
			this.status($(presence).find("status").text());
		}
	};

	return Contact;
});
