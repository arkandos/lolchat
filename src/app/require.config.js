
var require = {
	baseUrl: ".",
	paths: {
		"jquery": "bower_modules/jquery/dist/jquery.min",
		"bootstrap": "bower_modules/components-bootstrap/js/bootstrap.min",

		"crossroads": "bower_modules/crossroads/dist/crossroads.min",
		"hasher": "bower_modules/hasher/dist/js/hasher.min",

		"knockout": "bower_modules/knockout/dist/knockout",
		"knockout-projections": "bower_modules/knockout-projections/dist/knockout-projections.min",
		"knockout-es5": "bower_modules/knockout-es5/dist/knockout-es5.min",

		"strophe": "bower_modules/strophe/strophe.min",

		"signals": "bower_modules/js-signals/dist/signals.min",
		"text": "bower_modules/requirejs-text/text",
		"domReady": "bower_modules/requirejs-domready/domReady",
	},
	shim: {
		"bootstrap": [ "jquery" ],
		"knockout-projections": ["knockout"],
		"knockout-es5": ["knockout"],

		"strophe": {
			exports: "Strophe",
		},
	},
	waitSeconds : 30,
};
