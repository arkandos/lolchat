define(["jquery", "knockout", "strophe", "app/regions", "./connection.contact", "./connection.group", "knockout-projections"], function($, ko, Strophe, Regions, Contact, Group) {

	//CLASS Connection

	function Connection(username, password, region) {

		this.connection = new Strophe.Connection(Connection.BOSH_SERVICE);
		this.connectionStatus = ko.observable(Strophe.Status.DISCONNECTED);

		this.username = ko.unwrap(username);
		this.password = ko.unwrap(password);
		this.region = ko.unwrap(region);

		this.title = this.username;

		this._allGroups = ko.observableArray([]);
		this.groups = this._allGroups.filter(function(group) {
			return group.members().length > 0;
		});
		this.onlineGroups = this._allGroups.filter(function(group) {
			return group.onlineMembers().length > 0;
		});

		this.contacts = ko.observableArray([]);
		this.ownContact = null;

		this.messages = ko.observableArray([]);

		this.prematurePresences = [];

		this.connection.rawInput = function(data) { console.debug("RECV: " + data); };
		this.connection.rawOutput = function(data) { console.debug("SENT: " + data); };

	};

	Connection.byUsername = function(username) {
		username = ko.unwrap(username);
		return function(connection) {
			return connection.username == username;
		};
	};

	Connection.prototype.connect = function() {
		this.connection.connect(this.username + "@pvp.net", "AIR_" + this.password, this.onConnect.bind(this), 60, 1, this.region.route);
	};

	Connection.prototype.sendMessage = function(message, contact) {
		if(message.length == 0) return null;

		var msgXml = $msg({ to: contact.jid, from: this.ownContact.jid, type: 'chat' })
				.c("body").t(message).up();

		this.connection.send(msgXml.tree());

		//return a message object
		var msg = {
				from: this.ownContact.jid,
				to: contact.jid,
				message: message,
				fromContact: this.ownContact,
				toContact: contact,
				date: new Date()
			};
		this.messages.push(msg);
		return msg;
	};

	Connection.prototype.onConnect = function(status) {

		if(status == Strophe.Status.CONNECTED) {
			this.connection.addHandler(this.onMessage.bind(this), null, 'message', 'chat', null,  null);
			this.connection.addHandler(this.onPresence.bind(this), null, 'presence', null, null, null);
			this.connection.addHandler(this.onRoster.bind(this), null, 'iq', 'set', null, null);

			// send initial (empty) presence
			this.connection.send($pres().tree());
			// ooohhhh, here comes the roster, YEAAAHHH!
			this.connection.sendIQ($iq({ type: 'get' }).c('query', { xmlns: 'jabber:iq:roster' }), this.onRoster.bind(this));
			//we are here, jid is now different and contains our summoner id!
			this.createOwnContact();
		} else if(status == Strophe.Status.DISCONNECTED) {
			this.connection.reset();
		}

		this.connectionStatus(status);
		return true;
	};

	Connection.prototype.createOwnContact = function() {
		var ownContact = new Connection.Contact(this.region, this.connection.jid, {});;

		ownContact.statusMessage.extend({ localStorage: "statusMessage." + this.username });

		ownContact.show("chat");
		ownContact.summoner.updateSummonerData();
		ownContact.summoner.updateLeagueData();

		//throttle status updates and hook them into Strophe
		ownContact.presence.extend({ timeout: 300, method: "notifyWhenChangesStop" }).subscribe(function(newPresence) {
			this.connection.send(newPresence.tree());
		}, this);

		this.ownContact = ownContact;
	};

	Connection.prototype.messagesFrom = function(jid) {
		jid = Strophe.getBareJidFromJid(jid);
		var ownJid = Strophe.getBareJidFromJid(this.ownContact.jid);

		return this.messages.filter(function(message) {
			return (Strophe.getBareJidFromJid(message.from) == jid &&
					Strophe.getBareJidFromJid(message.to) == ownJid);
		});
	};
	Connection.prototype.messagesTo = function(jid) {
		jid = Strophe.getBareJidFromJid(jid);
		var ownJid = Strophe.getBareJidFromJid(this.ownContact.jid);

		return this.messages.filter(function(message) {
			return (Strophe.getBareJidFromJid(message.to) == jid &&
					Strophe.getBareJidFromJid(message.from) == ownJid);
		});
	};
	Connection.prototype.messagesWith = function(jid) {
		jid = Strophe.getBareJidFromJid(jid);

		return this.messages.filter(function(message) {
			return (Strophe.getBareJidFromJid(message.to) == jid ||
					Strophe.getBareJidFromJid(message.from) == jid);
		});
	};

	Connection.prototype.onMessage = function(msg) {
		var to = msg.getAttribute('to'),
			from = msg.getAttribute('from'),
			elems = msg.getElementsByTagName('body');

		if (elems.length > 0) {
			var body = elems[0],
				msgText = Strophe.getText(body);

			this.messages.push({
				from: from,
				to: to,
				date: new Date(),
				message: msgText,
				fromContact: ko.utils.arrayFirst(ko.unwrap(this.contacts), Contact.byJid(from)),
				toContact: this.ownContact
			});

		}
		return true;
	};

	Connection.prototype.onPresence = function(presence) {

		//if this presence comes from ourselves, just discard it, we don't need it
		if(Strophe.getBareJidFromJid($(presence).attr("from")) == Strophe.getBareJidFromJid(this.ownContact.jid)) {
			return true;
		}

		//cache presences until we got a roster, that is, if we got contacts.
		if(this.contacts().length == 0) {

			this.prematurePresences.push(presence);

		} else {

			var summonerId = Connection.Contact.summonerIdForJid( $(presence).attr("from") );
			var contact = ko.utils.arrayFirst(ko.unwrap(this.contacts), Connection.Contact.bySummonerId(summonerId));

			if(contact !== null) contact.presence(presence);
		}

		return true;
	};

	Connection.prototype.onRoster = function(roster) {

		var $query = $(roster).find("query");
		if($query.length == 0 || $query.attr("xmlns") != "jabber:iq:roster") {
			return true;
		}

		var items = $(roster).find("item");
		for(var i = 0; i < items.length; ++i) {
			var jid = $(items[i]).attr("jid"),
				defaults = {
					name: $(items[i]).attr("name"),
					subscription: $(items[i]).attr("subscription"),
					group: $(items[i]).find("group").text()
				},
				groupPriority = $(items[i]).find("group").attr("priority");

			//create a new group if it doesnt exist
			var group = ko.utils.arrayFirst(ko.unwrap(this._allGroups), Group.byName(defaults.group));
			if(group !== null) {
				group.name(defaults.group);
				group.priority(groupPriority);
			} else {
				group = new Connection.Group(this, defaults.group, groupPriority);
				this._allGroups.push(group);
			}

			//create a new contact if it doesnt exist
			var contact = ko.utils.arrayFirst(ko.unwrap(this.contacts), Contact.byJid(jid));
			if(contact !== null) {
				contact.summoner.name(defaults.name);
				contact.subscription(defaults.subscription);
				contact.group(defaults.group);
			} else {
				this.contacts.push(new Connection.Contact(this.region, jid, defaults));
			}
		}

		//read previously cached premature presences.
		if(this.prematurePresences) {
			for(var i = 0; i < this.prematurePresences.length; ++i) {
				this.onPresence(this.prematurePresences[i]);
			}
			delete this.prematurePresences;
		}

		return true;
	};

	Connection.BOSH_SERVICE = "http://league-streams.com:5280/http-bind/";
	Connection.Status = Strophe.Status;

	//TODO: don't do that
	Connection.Regions = Regions;

	Connection.Contact = Contact;
	Connection.Group = Group;

	return Connection;
});
