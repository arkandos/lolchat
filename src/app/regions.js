define([], function() {

	var Regions = [
		{
			title: "North America",
			abbr: "na",
			route: "xmpp:chat.na1.lol.riotgames.com:5223",
			api: "https://na.api.pvp.net/api/lol/na",
		},
		{
			title: "Europe West",
			abbr: "euw",
			route: "xmpp:chat.euw1.lol.riotgames.com:5223",
			api: "https://euw.api.pvp.net/api/lol/euw",
		},
		{
			title: "Europe Nordic & East",
			abbr: "eune",
			route: "xmpp:chat.eun1.lol.riotgames.com:5223",
			api: "https://eune.api.pvp.net/api/lol/eune"
		}
	];

	Regions.byAbbr = function(abbr) {
		abbr = abbr.toLowerCase();
		for(var i = 0; i < Regions.length; ++i) {
			if(Regions[i].abbr.toLowerCase() == abbr) {
				return Regions[i];
			}
		};
		return null;
	};


	return Regions;
});
