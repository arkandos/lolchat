define(["knockout", "knockout-projections"], function(ko) {

	function Group(connection, groupName, priority) {
		//this.connection = connection;

		this.name = ko.observable(groupName);
		this.priority = ko.observable(priority);


		this.collapsed = ko.observable(false);
		this.title = ko.pureComputed(function() {
			var name = this.name();
			if(name == "**Default") return "Default";
			else return name;
		}, this);

		this.members = connection.contacts.filter(function(contact) {
			return contact.group() == this.name();
		}.bind(this));

		this.onlineMembers = connection.contacts.filter(function(contact) {
			var status = contact.show();
			return (contact.group() == this.name() && status.length > 0 && status != "offline");
		}.bind(this));
	};

	Group.byName = function(groupName) {
		return function(group) {
			return group.name() == groupName;
		};
	};

	return Group;
});
