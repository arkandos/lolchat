define(["knockout", "text!./profile-page.html", "app/regions", "app/summoner"], function(ko, template, Regions, Summoner) {

	function ProfilePageViewModel(appModel) {
		ko.utils.extend(this, appModel);

		this.summoner = new Summoner(this.route().summonerId, Regions.byAbbr(this.route().regionAbbr));
		this.summoner.updateAll(function(summoner) {
			this.loading(false);
		}.bind(this));

		this.loading = ko.observable(true);
	};

	ProfilePageViewModel.prototype.dispose = function() {

	};

	return { viewModel: ProfilePageViewModel, template: template };
});
