define(["jquery", "knockout", "text!./chat-page.html", "app/connection", "hasher"], function($, ko, template, Connection, hasher) {

	function MessagePanel(connection, contact) {
		this.connection = connection;
		this.contact = contact;

		this.currentMessage = ko.observable("");

		this.currentlyFocused = ko.observable(false);
		this.hasNewMessages = ko.observable(false);
		this.currentlyFocused.subscribe(function(newValue) {
			this.hasNewMessages(false);
		}, this);

		//this.recievedMessages = this.connection.messagesFrom(contact.jid);
		//this.sentMessages = this.connection.messagesTo(contact.jid);

		//TODO: THIS HAS TO BE DISPOSED
		this.messages = this.connection.messagesWith(contact.jid);
	};


	MessagePanel.byContact = function(contact) {
		return function(panel) {
			return panel.contact === contact;
		};
	};

	MessagePanel.prototype.sendMessage = function() {
		this.connection.sendMessage(this.currentMessage(), this.contact);
		this.currentMessage("");
	};

	MessagePanel.prototype.scrollToBottom = function() {
		var $messageArea = $("#chat-window-message-area");
		$messageArea.stop(true);
		$messageArea.animate({ scrollTop: $messageArea[0].scrollHeight }, 300);
	};

	MessagePanel.prototype.dispose = function() {
		this.messages.dispose();
	};


	function ChatViewModel(appModel) {
		ko.utils.extend(this, appModel);

		this.connection = ko.utils.arrayFirst(ko.unwrap(this.activeConnections), Connection.byUsername(this.route().username));
		//nothing to do here *flies away* (we display an error in the view template)
		if(this.connection === null) return;

		this.messagePanels = ko.observableArray([]);
		this.currentMessagePanel = ko.observable(null);
		//only notify us if the object actually changes
		this.currentMessagePanel.equalityComparer = function(a, b) { return a === b; };

		//update a observable in the message panel to indicate if this is the current message panel.
		this.currentMessagePanel.subscribe(function(oldValue) {
			if(oldValue !== null) oldValue.currentlyFocused(false);
		}, null, "beforeChange");
		this.currentMessagePanel.subscribe(function(newValue) {
			if(newValue !== null) newValue.currentlyFocused(true);
		});

		// if a message is added, try to open a message pane for it
		//TODO: this is ugly
		this.messsagesSubscription = this.connection.messages.subscribe(function(changes) {
			for(var i = 0; i < changes.length; ++i) {
				var change = changes[i];
				if(change.status == "added") {
					if(change.value.fromContact !== this.connection.ownContact) {
						var panel = this.ensureMessagePanel(change.value.fromContact);
						if(!panel.currentlyFocused()) panel.hasNewMessages(true);
					}
				}
			}
		}, this, "arrayChange");

		//open previous message panels again.
		if(ChatViewModel.openedMessagePanels.hasOwnProperty(this.connection.username)) {
			for(var i = 0, a = ChatViewModel.openedMessagePanels[this.connection.username];
				i < a.length;
				++i)
			{
				var contact = ko.utils.arrayFirst(ko.unwrap(this.connection.contacts), Connection.Contact.byJid(a[i]));
				if(contact) {
					this.ensureMessagePanel(contact);
				}
			}
		}
		if(ChatViewModel.focusedMessagePanel.hasOwnProperty(this.connection.username)) {
			var panelJid =  ChatViewModel.focusedMessagePanel[this.connection.username];
			if(panelJid.length > 0) {
				var contact = ko.utils.arrayFirst(ko.unwrap(this.connection.contacts), Connection.Contact.byJid(panelJid));
				if(contact) {
					this.currentMessagePanel(this.ensureMessagePanel(contact));
				}
			}
		}


		$('body').on('click', '[data-toggle=collapse-next]', function (e) {
			var $target = $(this).parents('.panel').find('.panel-collapse');
			$target.collapse('toggle');
		});

		this.onContactClicked = this.onContactClicked.bind(this);
		this.closeMessagePanel = this.closeMessagePanel.bind(this);
	};

	ChatViewModel.openedMessagePanels = {};
	ChatViewModel.focusedMessagePanel = {};

	ChatViewModel.prototype.ensureMessagePanel = function(contact) {
		var panel = ko.utils.arrayFirst(ko.unwrap(this.messagePanels), MessagePanel.byContact(contact));
		if(panel === null) { //this panel doesn't exist yet, so create a new one please
			panel = new MessagePanel(this.connection, contact);
			this.messagePanels.push(panel);
		}
		return panel;
	};

	ChatViewModel.prototype.onContactClicked = function(contact, event) {
		var panel = this.ensureMessagePanel(contact);
		this.currentMessagePanel(panel);

		//event.preventDefault();
	};

	ChatViewModel.prototype.closeMessagePanel = function(messagePanel) {
		var id = this.messagePanels.remove(messagePanel);
		if(this.currentMessagePanel() === messagePanel) {
			if(this.messagePanels().length > 0) {
				this.currentMessagePanel(this.messagePanels()[id - 1]);
			} else {
				this.currentMessagePanel(null);
			}
		}
		messagePanel.dispose();
	};

	ChatViewModel.prototype.logout = function() {
		this.connection.connection.disconnect();
		hasher.setHash("");
	};

	ChatViewModel.prototype.toggleShowStatus = function() {
		var show = (this.connection.ownContact.show() == "chat");
		this.connection.ownContact.show(show ? "away" : "chat");
	};

	ChatViewModel.prototype.dispose = function() {
		if(this.connnection === null) return;

		var messagePanels = this.messagePanels(),
			connectionStatus = this.connection.connectionStatus(),
			currentMessagePanel = this.currentMessagePanel();

		this.messsagesSubscription.dispose();
		for(var i = 0; i < messagePanels.length; ++i)
		{
			messagePanels[i].dispose();
		}

		if (connectionStatus == Connection.Status.CONNECTED) {
			ChatViewModel.openedMessagePanels[this.connection.username] =
				ko.utils.arrayMap(messagePanels, function(panel) {
					return panel.contact.jid;
				});

			if(currentMessagePanel) {
				ChatViewModel.focusedMessagePanel[this.connection.username] = currentMessagePanel.contact.jid;
			} else {
				ChatViewModel.focusedMessagePanel[this.connection.username] = "";
			}
		} else {
			ChatViewModel.openedMessagePanels[this.connection.username] = [];
			ChatViewModel.focusedMessagePanel[this.connection.username] = "";
		}
	};

	return { viewModel: ChatViewModel, template: template };
});
