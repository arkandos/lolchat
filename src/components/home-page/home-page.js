define(["knockout", "text!./home-page.html", "hasher", "app/connection"], function(ko, template, hasher, Connection) {

	function HomeViewModel(appModel) {
		ko.utils.extend(this, appModel);

		this.flashLevel = ko.observable("info");
		this.flashMessage = ko.observable("");

		this.regions = Connection.Regions;

		this.region = ko.observable(this.regions[0]);
		this.username = ko.observable("");
		this.password = ko.observable("");

		//save the region using the abbr as a unique key, just saving the object doesn't really do it
		this.regionUpdater = ko.computed({
			read: function() {
				var region = this.region();
				if(region) return region.abbr;
				else return "";
			},
			write: function(abbr) {
				this.region(ko.utils.arrayFirst(this.regions, function(region) {
					return region.abbr == abbr;
				}));
			},
			owner: this,
		}).extend({ localStorage: "region" });

	};

	HomeViewModel.prototype.login = function() {

		//check if we already got a route
		var connection = ko.utils.arrayFirst(ko.unwrap(this.activeConnections), Connection.byUsername(this.username));

		if(connection !== null) {
			hasher.setHash("chat/" + connection.username);
		} else {

			var connection = this.createConnection();
			connection.connect();
		}
	};

	HomeViewModel.prototype.createConnection = function() {
		var connection = new Connection(this.username, this.password, this.region);

		var statusSubscription = connection.connectionStatus.subscribe(function(status) {

			if(status == Connection.Status.ATTACHED || status ==  Connection.Status.CONNECTED) {
				this.setFlash("", "");
				this.activeConnections.push(connection);
				hasher.setHash("chat/" + connection.username);
			} else if(status == Connection.Status.CONNECTING || status == Connection.Status.AUTHENTICATING) {
				this.setFlash("info", "Connecting...");
			} else if(status == Connection.Status.DISCONNECTING) {
				this.setFlash("info", "Disconnecting...");
			} else {
				// something finished and it is not that we successfully connected
				if(status == Connection.Status.ERROR || status == Connection.Status.CONNFAIL) {
					//TODO
					this.setFlash("danger", "Error connecting to the chat server. If this problem persists, please contact me.");
				} else if (status == Connection.Status.AUTHFAIL) {
					//TODO
					this.setFlash("warning", "Invalid login information, please try again!");
				} else if(status == Connection.Status.DISCONNECTED) {
					this.setFlash("", "");
				} else {
					this.setFlash("danger", "ERROR UNKNOWN CONNECTION CODE " + status);
				}

				this.activeConnections.remove(connection);
				statusSubscription.dispose(); // we are done with this
			}

		}, this);

		return connection;
	};

	HomeViewModel.prototype.setFlash = function(flashLevel, flashMessage) {
		this.flashLevel(flashLevel);
		this.flashMessage(flashMessage);
	};

	HomeViewModel.prototype.dispose = function() {
		this.regionUpdater.dispose();
	};

	return { viewModel: HomeViewModel, template: template };

});
