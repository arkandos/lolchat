define(["knockout", "text!./alert.html"], function(ko, template) {

	function AlertViewModel(params) {

		this.level = ko.pureComputed(function() {
			return ko.unwrap(params.level);
		});
		this.message = ko.pureComputed(function() {
			return ko.unwrap(params.message);
		});
		this.dismissible = ko.pureComputed(function() {
			return ko.unwrap(params.dismissible);
		});

		this.alertClass = ko.pureComputed(function() {
			var level = ko.unwrap(this.level),
				dismissible = ko.unwrap(this.dismissible);

			var classes = ["alert"];

			if(level && level.length > 0) classes.push("alert-" + level);
			else return classes.push("alert-info");

			if(dismissible) classes.push("alert-dismissible");

			return classes.join(" ");
		}, this);
	};

	return { viewModel: AlertViewModel, template: template };
});
