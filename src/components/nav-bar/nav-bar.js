define(["knockout", "text!./nav-bar.html", "hasher"], function(ko, template, hasher) {

	function NavBarViewModel(params) {
		this.route = params.route;
		this.connections = params.connections;
	};

	NavBarViewModel.prototype.logout = function(connection, event) {
		connection.connection.disconnect();
		hasher.setHash("");
	};

	NavBarViewModel.prototype.logoutAll = function() {
		var connections = this.connections();
		for(var i = 0; i < connections.length; ++i) {
		//while(connections.length > 0) {
			connections[i].connection.disconnect();
		}
		hasher.setHash("");
	};


	return { viewModel: NavBarViewModel, template: template };
});
